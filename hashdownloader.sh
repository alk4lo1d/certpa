#!/bin/bash
BASEURL="https://infosec.cert-pa.it"
HASHLIST="hashlist.txt"
USERAGENT="Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko"

echo "#######################################################################################"
echo "CERT-PA Hashlist Downloader"
echo "#######################################################################################"
echo ""
echo "#######################################################################################"

if [ ! -f $HASHLIST ]; then
	echo "Creating master hashlist file..."
	cat /dev/null > $HASHLIST
	echo "Created"
fi

echo "#######################################################################################"

echo "Downloading hashes...."

for i in $(curl -s -A "$USERAGENT" "https://infosec.cert-pa.it/analyze/submission.html" | grep -o -E "/analyze/submission-page-([0-9]{1,3}).html"); do
    curl -s -A "$USERAGENT" $BASEURL$i |  grep -o -E '[A-Fa-f0-9]{32}' | sort -u >> "$HASHLIST"
done

echo "Downloaded"

echo "#######################################################################################"